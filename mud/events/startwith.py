# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class StartWithEvent(Event3):
	NAME = "start-with"

	def perform(self):
		if not self.object.has_prop("startable"):
			self.fail()
			return self.inform("start-with.failed-not-startable")
		elif not self.object2.has_prop("key-for-moto") and not self.object.has_prop("broken"):
			self.fail()
			return self.inform("start-with.failed-not-for-moto")
		elif self.object.has_prop("startable") and not self.object.has_prop("turned-off"):
			self.fail()
			return self.inform("start-with.failed-already-started")
		self.inform("start-with")
