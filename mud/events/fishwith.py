# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class FishWithEvent(Event3):
	NAME = "fish"

	def perform(self):
		if not self.object.has_prop("fishable"):
			self.fail()
			return self.inform("fish.failed-not-fishable")
		elif not self.object2.has_prop("for-fish"):
			self.fail()
			return self.inform("fish.failed-not-for-fish")
		if self.object in self.actor:
			self.add_prop("object-already-in-inventory")
			return self.fish_failed()
		self.object.move_to(self.actor)
		self.inform("fish")
		
	def fish_failed(self):
		self.fail()
		self.inform("take.failed")
