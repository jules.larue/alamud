# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class RepairWithEvent(Event3):
	NAME = "repair"

	def perform(self):
		if not self.object.has_prop("repairable"):
			self.fail()
			return self.inform("repair.failed-not-repairable")
		elif not self.object2.has_prop("for-repair"):
			self.fail()
			return self.inform("repair.failed-not-for-repair")
		elif self.object.has_prop("repairable") and not self.object.has_prop("broken") and self.object2.has_prop("for-repair"):
			self.fail()
			return self.inform("repair.failed-already-repaired")
		self.inform("repair")
