# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class TalkEvent(Event2):
	NAME = "talk"

	def perform(self):
		if self.object.has_prop("person"):
			self.buffer_clear()
			self.buffer_inform("talk.actor")
			self.actor.send_result(self.buffer_get())
		else:
			self.fail()
			return self.inform("talk.failed")
