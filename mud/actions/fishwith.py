# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import FishWithEvent

class FishWithAction(Action3):
    EVENT = FishWithEvent
    ACTION = "fish"
    RESOLVE_OBJECT="find_for_take"
    RESOLVE_OBJECT2 = "resolve_for_use"
